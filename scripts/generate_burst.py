from numpy import array
import numpy as np

target_sampling_rate = 44100
target_folder = "./data/" + str(target_sampling_rate) + "/"
target_file = "burst.dat"
target_path = target_folder + target_file
print("Target Path -> {}".format(target_path))

floats = np.random.rand(target_sampling_rate)

float_array = array(floats,'float32')
output_file = open(target_path, 'wb')
float_array.tofile(output_file)
output_file.close()
