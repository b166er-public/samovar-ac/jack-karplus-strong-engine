from numpy import array
import numpy as np

target_sampling_rate = 44100
target_folder = "./data/" + str(target_sampling_rate) + "/"
target_file = "kernel.dat"
target_path = target_folder + target_file
print("Target Path -> {}".format(target_path))

floats_old = [
    -0.02010411882885732,
    -0.05842798004352509,
    -0.061178403647821976,
    -0.010939393385338943,
    0.05125096443534972,
    0.033220867678947885,
    -0.05655276971833928,
    -0.08565500737264514,
    0.0633795996605449,
    0.310854403656636,
    0.4344309124179415,
    0.310854403656636,
    0.0633795996605449,
    -0.08565500737264514,
    -0.05655276971833928,
    0.033220867678947885,
    0.05125096443534972,
    -0.010939393385338943,
    -0.061178403647821976,
    -0.05842798004352509,
    -0.02010411882885732
]

floats = [
    0.05,
    0.05,
    0.04,
    0.05,
    0.05,
    0.05,
    0.05,
    0.06,
    0.05,
    0.05,
    0.05,
    0.03,
    0.05,
    0.05,
    0.05,
    0.05,
    0.05,
    0.04,
    0.05,
    0.05,
]

print("Kernel Sum -> {}".format(np.sum(floats)))

float_array = array(floats,'float32')
output_file = open(target_path, 'wb')
float_array.tofile(output_file)
output_file.close()
