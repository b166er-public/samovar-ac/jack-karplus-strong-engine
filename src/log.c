#include <stdio.h>

#include "log.h"

void
jks_log_info(const char* function, const char* message) {
    fprintf(stdout, "[I] [%s] %s\n", function, message);
}

void
jks_log_warning(const char* function, const char* message) {
    fprintf(stdout, "[W] [%s] %s\n", function, message);
}

void
jks_log_error(const char* function, const char* message) {
    fprintf(stderr, "[E] [%s] %s\n", function, message);
}