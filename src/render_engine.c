#include <stddef.h>
#include <string.h>
#include <math.h>

#include <jack/jack.h>
#include <jack/midiport.h>

#include "log.h"
#include "render_engine.h"

// Update MIDI delta
void
jks_render_engine_provider_update_delta(
    jks_render_engine_t* obj,
    unsigned int* midi_delta
) {
    pthread_mutex_lock(&obj->midi_state_mutex);
    
    for(int k=0; k < NUMBER_OF_MIDI_KEYS; k++) {
        if( (obj->midi_state_old[k] == 0) && (obj->midi_state_new[k] != 0)) {
            midi_delta[k] = obj->midi_state_new[k];
        } else {
            midi_delta[k] = 0;
        }
    }
    
    memcpy(
        obj->midi_state_old, 
        obj->midi_state_new,
        sizeof(unsigned int)*NUMBER_OF_MIDI_KEYS
    );
    
    pthread_mutex_unlock(&obj->midi_state_mutex);
}

// Start new note instance handler
void
jks_render_engine_provider_start_new_instance(
    jks_render_engine_t* obj,
    const float note_frequency,
    const float amplitude
) {
    int free_idx = -1;
    for(int i=0; i<MAX_NOTE_INSTANCES; i++) {
        if(obj->instances[i] == NULL) {
            free_idx = i;
            break;
        }
    }
    
    if(free_idx >= 0) {
        obj->instances[free_idx] = jks_note_instance_create(
            note_frequency,
            obj->sampling_rate,
            obj->burst,
            obj->burst_size,
            obj->kernel,
            obj->kernel_size
        );
    } else {
        jks_log_warning(__FUNCTION__, "no more space for further note instances");
    }
}

// Rendering function
void*
jks_render_engine_provider_thread_function(void *arg) {
    jks_render_engine_t* obj = (jks_render_engine_t*)arg;
    
    unsigned int midi_delta[NUMBER_OF_MIDI_KEYS];
    float* local_buffer = (float*) malloc( sizeof(float) * (obj->buffer_size) );
    
    while(1) {
        pthread_mutex_lock(&obj->rendering_request_mutex);
        
        // Get newest MIDI states
        jks_render_engine_provider_update_delta(
            obj,
            midi_delta
        );
        
        // Start note instances where needed
        for(int k=0; k < NUMBER_OF_MIDI_KEYS; k++) {
            unsigned int key_delta = midi_delta[k];
            if(key_delta > 0) {
                jks_render_engine_provider_start_new_instance(
                    obj,
                    440.0 * pow(2.0, (((float)k)-69.0) / 12),
                    ((float)key_delta) / 127.0
                );
                jks_log_info(__FUNCTION__, "Start new not instance!");
            }
        }
        
        // Start writting to buffer
        pthread_mutex_lock(&obj->buffer_mutex);
        
        int buffer_size = obj->buffer_size;
        float* buffer = obj->buffer;
        
        // Clear local buffer
        for(int b=0; b < buffer_size; b++) {
            buffer[b] = 0.0;
        }
        
        // Render buffer
        for(int i=0; i < MAX_NOTE_INSTANCES; i++) {
            if(obj->instances[i] != NULL) {
                jks_note_instance_render_add_array(
                    obj->instances[i],
                    buffer,
                    obj->buffer_size
                );
            }
        }
        
        // Stop writting to buffer
        pthread_mutex_unlock(&obj->buffer_mutex);
        
        // Kill silente note instances
        for(int i=0; i < MAX_NOTE_INSTANCES; i++) {
            if(obj->instances[i] != NULL) {
                if(obj->instances[i]->silece_sample_counter > 10000) {
                    jks_note_instance_destroy(obj->instances[i]);
                    obj->instances[i] = NULL;
                    jks_log_info(__FUNCTION__, "Note instance died!");
                }
            }
        }

    }
    
    return 0;
}

// Consuming function (invoked by JACK) - onProcess
int
jks_render_engine_consumer_on_process(
    jack_nframes_t nframes,
    void *arg
) {
    jks_render_engine_t* obj = (jks_render_engine_t*)arg;
    
    // Update MIDI keys
    void* buffer_midi_in = jack_port_get_buffer(
        obj->jack_midi_input_port, 
        nframes
    );
    pthread_mutex_lock(&obj->midi_state_mutex);
    jack_nframes_t midi_event_count = jack_midi_get_event_count(buffer_midi_in);
    jack_midi_event_t current_midi_event;
    for(int e=0; e < midi_event_count; e++) {
        jack_midi_event_get(
            &current_midi_event, 
            buffer_midi_in, 
            e
        );
        
        uint8_t evt_type = current_midi_event.buffer[0] & 0xf0;
        uint8_t evt_channel = current_midi_event.buffer[0] & 0xf;
        
        switch (evt_type) {
            case 0x90:
                obj->midi_state_new[ current_midi_event.buffer[1] ] = current_midi_event.buffer[2];
                break;
            case 0x80:
                obj->midi_state_new[ current_midi_event.buffer[1] ] = 0;
                break;
            default:
                break;
        }
    }
    pthread_mutex_unlock(&obj->midi_state_mutex);
    
    // Copy last rendered buffer
    jack_default_audio_sample_t* buffer_pcm_out = jack_port_get_buffer(
        obj->jack_pcm_output_port, 
        nframes
    );
    pthread_mutex_lock(&obj->buffer_mutex);
    for(int s=0; s < nframes; s++) {
        buffer_pcm_out[s] = obj->buffer[s];
    }
    pthread_mutex_unlock(&obj->buffer_mutex);
    
    // Trigger calculation of next buffer
    pthread_mutex_unlock(&obj->rendering_request_mutex);
    
    return 0;
}

// Consuming function (invoked by JACK) - onShutdown
void
jks_render_engine_consumer_on_shutdown(void *arg) {
    exit(1);
}

jks_render_engine_t*
jks_render_engine_create(
    float* burst,
    unsigned int burst_size,
    float* kernel,
    unsigned int kernel_size
) {
    jks_render_engine_t* obj = (jks_render_engine_t*) malloc(sizeof(jks_render_engine_t));
    
    // Open JACK client
    obj->jack_client_handle = jack_client_open(
        "karplus_strong", /* client name*/
        JackNullOption, /* options */
        NULL, /* status */
        NULL /* server name */
    );
    
    // Create MIDI IN Port
    obj->jack_midi_input_port = jack_port_register(
        obj->jack_client_handle, 
        "midi-in",
        JACK_DEFAULT_MIDI_TYPE,
        JackPortIsInput, 
        0
    );
    
    // Create PCM OUT Port
    obj->jack_pcm_output_port = jack_port_register(
        obj->jack_client_handle, 
        "pcm-out",
        JACK_DEFAULT_AUDIO_TYPE,
        JackPortIsOutput, 
        0
    );

    obj->sampling_rate = jack_get_sample_rate(obj->jack_client_handle);
    
    // Copy burst
    obj->burst = (float*) malloc(burst_size * sizeof(float));
    memcpy(
        obj->burst, 
        burst, 
        sizeof(float)*burst_size
    );
    obj->burst_size = burst_size;
    
    // Copy kernel
    obj->kernel = (float*) malloc(kernel_size * sizeof(float));
    memcpy(
        obj->kernel, 
        kernel, 
        sizeof(float)*kernel_size
    );
    obj->kernel_size = kernel_size;
    
    // MIDI state
    for(int m=0; m < NUMBER_OF_MIDI_KEYS; m++) {
        obj->midi_state_new[m] = 0;
        obj->midi_state_old[m] = 0;
    }
    
    // Rendering buffer
    obj->buffer_size = jack_get_buffer_size(obj->jack_client_handle);
    obj->buffer = (float*) malloc(obj->buffer_size * sizeof(float));
    pthread_mutex_init(&(obj->buffer_mutex), NULL);
    
    // Instance management
    for(int i=0; i < MAX_NOTE_INSTANCES; i++) {
        obj->instances[i] = NULL;
    }
    
    // Rendering request mutex
    pthread_mutex_init(&(obj->rendering_request_mutex), NULL);
    pthread_mutex_lock(&obj->rendering_request_mutex);
    
    // Provider thread
    pthread_create(
        &obj->provider_thread,
        NULL,
        &jks_render_engine_provider_thread_function,
        obj
    );
    
    // Register JACK callbacks (on-process and on-shutdown)
    jack_set_process_callback(
        obj->jack_client_handle, 
        jks_render_engine_consumer_on_process, 
        obj
    );
    jack_on_shutdown(
        obj->jack_client_handle, 
        jks_render_engine_consumer_on_shutdown, 
        0
    );

    // Initialize killer mutex
    pthread_mutex_init(&obj->killer_mutex, NULL);
    pthread_mutex_lock(&obj->killer_mutex);

    // Activate JACK Client
    if (jack_activate(obj->jack_client_handle)) {
        jks_log_error(__FUNCTION__, "JACK Client starting ... FAILED!");
        exit(1);
    } else {
        jks_log_info(__FUNCTION__, "JACK Client starting... OK\n");
    }

    return obj;
}

void
jks_render_engine_destroy(jks_render_engine_t* obj) {
    jack_client_close(obj->jack_client_handle);
}

void
jks_render_engine_request_termination(jks_render_engine_t* obj) {
    pthread_mutex_unlock(&obj->killer_mutex);
}