#include <stdlib.h>
#include <stdio.h>

#include "log.h"
#include "float_array.h"

jks_float_array_t*
jks_float_array_load(const char* path) {
    jks_float_array_t* obj = (jks_float_array_t*) malloc(sizeof(jks_float_array_t));
    
    FILE *float_array_file = fopen(path, "rb");
    fseek(float_array_file, 0, SEEK_END);
    long float_array_file_size = ftell(float_array_file);
    fseek(float_array_file, 0, SEEK_SET); 

    obj->data = (float*) malloc(float_array_file_size);
    obj->size = float_array_file_size/sizeof(float);
    fread(
        obj->data, 
        4, 
        obj->size, 
        float_array_file
    );
    
    fclose(float_array_file);
    float_array_file = NULL;
    
    return obj;
}

void
jks_float_array_destroy(jks_float_array_t* obj) {
    if(obj != NULL) {
        obj->size = 0;
        if(obj->data != NULL) {
            free(obj->data);
            obj->data = NULL;
        }
    } else {
        jks_log_error(__FUNCTION__, "received NULL pointer");
    }
}