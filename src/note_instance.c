#include <math.h>

#include "log.h"
#include "note_instance.h"

jks_note_instance_t*
jks_note_instance_create(
    const float freq,
    const unsigned int sampling_rate,
    const float* burst,
    const unsigned int burst_size,
    const float* kernel,
    const unsigned int kernel_size
) {
    const unsigned int memory_size = (unsigned int) (((float)sampling_rate) / freq);
    jks_note_instance_t* obj = (jks_note_instance_t*) malloc(sizeof(jks_note_instance_t));
    obj->freq = freq;
    obj->memory = jks_ring_buffer_create(memory_size);
    jks_ring_buffer_init(
        obj->memory,
        0,
        burst
    );
    
    obj->filter = jks_fir_filter_create(kernel_size, kernel);
    obj->silece_sample_counter = 0;
}

void
jks_note_instance_destroy(
    jks_note_instance_t* obj
) {
    obj->freq = 0.0;
    if(obj->memory != NULL) {
        jks_ring_buffer_destroy(obj->memory);
        obj->memory = NULL;
    }
    
    if(obj->filter != NULL) {
        jks_fir_filter_destroy(obj->filter);
        obj->filter = NULL;
    }
    
    obj->silece_sample_counter = 0;
}

float
jks_note_instance_render_single(
    jks_note_instance_t* obj
) {
    float y = jks_ring_buffer_read_single(obj->memory);
    float z = jks_fir_filter_process_single(obj->filter, y);
    jks_ring_buffer_write_single(obj->memory, z);
    
    float yabs = fabsf(y);
    if(yabs < 0.001) {
        obj->silece_sample_counter++;
    } else if(yabs > 1.0) {
        jks_log_warning(__FUNCTION__, "Note clipped!");
    } else {
        obj->silece_sample_counter = 0;
    }
    
    return y;
}

void
jks_note_instance_render_add_array(
    jks_note_instance_t* obj,
    float* output,
    const unsigned int output_size
) {
    for(int o=0; o<output_size; o++) {
        output[o] += jks_note_instance_render_single(obj);
    }
}
