#include <string.h>

#include "log.h"
#include "fir_filter.h"

jks_fir_filter_t*
jks_fir_filter_create(const unsigned int kernel_size, const float* kernel) {
    jks_fir_filter_t* obj = malloc(sizeof(jks_fir_filter_t));
    obj->kernel_size = kernel_size;
    obj->kernel = (float*) malloc(sizeof(float)*kernel_size);
    memcpy(
        obj->kernel, 
        kernel, 
        sizeof(float)*kernel_size
    );
    obj->state = jks_ring_buffer_create(kernel_size);
    obj->evaluate_cache = (float*) malloc(sizeof(float)*kernel_size);
    return obj;
}

void
jks_fir_filter_destroy(jks_fir_filter_t* obj) {
    if(obj != NULL) {
        obj->kernel_size = 0;
        if(obj->kernel != NULL) {
            free(obj->kernel);
            obj->kernel = NULL;
        }
        
        if(obj->state != NULL) {
            jks_ring_buffer_destroy(obj->state);
            obj->state = NULL;
        }
        
        if(obj->evaluate_cache != NULL) {
            free(obj->evaluate_cache);
            obj->evaluate_cache = NULL;
        }
    } else {
        jks_log_error(__FUNCTION__, "received NULL pointer");
    }
}

float
jks_fir_filter_process_single(jks_fir_filter_t* obj, float input) {
    const unsigned int kernel_size = obj->kernel_size;
    
    jks_ring_buffer_write_single(
        obj->state,
        input
    );
    
    jks_ring_buffer_read_array(
        obj->state, 
        obj->evaluate_cache, 
        kernel_size
    );
    
    float result = 0.0;
    for(int k=0; k < kernel_size; k++) {
        result += (obj->evaluate_cache[k] * obj->kernel[k]);
    }
    
    return result;
}

void
jks_fir_filter_process_array(
    jks_fir_filter_t* obj, 
    const float* input,
    const unsigned int input_size,
    float* output
) {
    for(int i=0; i < input_size; i++) {
        float v = jks_fir_filter_process_single(
            obj, 
            input[i]
        );
        *(output+i) = v;
    }
}
