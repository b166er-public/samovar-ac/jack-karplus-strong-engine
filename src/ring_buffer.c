#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "log.h"
#include "ring_buffer.h"

jks_ring_buffer_t*
jks_ring_buffer_create(unsigned int size) {
    jks_ring_buffer_t* obj = malloc(sizeof(jks_ring_buffer_t));
    obj->size = size;
    obj->pos = 0;
    obj->data = (float *) calloc(size, sizeof(float));
    
    return obj;
}

void
jks_ring_buffer_destroy(jks_ring_buffer_t* obj) {
    if(obj != NULL) {
        obj->size = 0;
        obj->pos = 0;
        if(obj->data != NULL) {
            free(obj->data);
            obj->data = NULL;
        }
    } else {
        jks_log_error(__FUNCTION__, "received NULL pointer");
    }
}

void
jks_ring_buffer_init(
    jks_ring_buffer_t* obj,
    const unsigned int pos,
    const float* data
) {
    obj->pos = pos;
    memcpy(
        obj->data, 
        data, 
        obj->size 
    );
}

/* I/O single value */
float
jks_ring_buffer_read_single(const jks_ring_buffer_t* obj) {
    return obj->data[obj->pos];
}

float
jks_ring_buffer_read_single_offset(const jks_ring_buffer_t* obj, const unsigned int offset) {
    return obj->data[(obj->pos + offset) % (obj->size)];
}

void
jks_ring_buffer_write_single(jks_ring_buffer_t* obj, float value) {
    const unsigned int pos = obj->pos;
    obj->data[pos] = value;
    obj->pos = (pos+1) % obj->size;
}

/* I/O quads */
float_quad_t
jks_ring_buffer_read_quad_offset(const jks_ring_buffer_t* obj, const unsigned int offset) {
    const int size = obj->size;
    const float* data = obj->data;
    const int p0 = (obj->pos + offset) % size;
    const int p1 = (p1+1) % size;
    const int p2 = (p1+2) % size;
    const int p3 = (p1+3) % size;
    float_quad_t result;
    result.f0 = data[p0];
    result.f1 = data[p1];
    result.f2 = data[p2];
    result.f3 = data[p3];
    return result;
}

float_quad_t
jks_ring_buffer_read_quad(const jks_ring_buffer_t* obj) {
    return jks_ring_buffer_read_quad_offset(obj, 0);
}

void
jks_ring_buffer_write_quad(jks_ring_buffer_t* obj, const float_quad_t value) {
    const unsigned int size = obj->size;
    float* data = obj->data;
    
    const unsigned int p0 = obj->pos;
    const unsigned int p1 = (p0+1) % size;
    const unsigned int p2 = (p0+2) % size;
    const unsigned int p3 = (p0+3) % size;
    
    data[p0] = value.f0;
    data[p1] = value.f1;
    data[p2] = value.f2;
    data[p3] = value.f3;
    
    obj->pos = (p3+1) % size;
}

/* I/O array */
void
jks_ring_buffer_read_array(
    const jks_ring_buffer_t* obj, 
    float* target, 
    const unsigned int target_size
) {
    const unsigned int size = obj->size;
    if(target_size <= size) {
        const unsigned int read_quads = target_size / 4;
        const unsigned int read_single = target_size % 4;
        
        float* target_pos = target;
        
        // Read quads
        float_quad_t tmp_quad;
        for(int q=0; q < read_quads; q++) {
            tmp_quad = jks_ring_buffer_read_quad_offset(obj, q*4);
            *(target_pos) = tmp_quad.f0;
            *(target_pos+1) = tmp_quad.f1;
            *(target_pos+2) = tmp_quad.f2;
            *(target_pos+3) = tmp_quad.f3;
            target_pos += 4;
        }
        
        // Read single values
        float tmp_single;
        unsigned int offset = read_quads*4;
        for(int s=0; s < read_single; s++) {
            tmp_single = jks_ring_buffer_read_single_offset(obj, offset);
            *target_pos = tmp_single;
            target_pos++;
            offset++;
        }
    } else {
        jks_log_error(__FUNCTION__, "can not read more bytes than placed in ring buffer");
    }
}