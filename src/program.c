/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// ----------------------------------------------------------------------------
// Includes
// ----------------------------------------------------------------------------

#include <signal.h>

#include "common.h"
#include "fir_filter.h"
#include "float_array.h"
#include "log.h"
#include "note_instance.h"
#include "render_engine.h"
#include "ring_buffer.h"

static jks_render_engine_t* gbl_engine = NULL;

typedef struct jks_configuration {
    char* burst_data_path;
    char* kernel_data_path;
} jks_configuration_t;

jks_configuration_t*
jks_configuration_parse() {
    jks_configuration_t* obj = (jks_configuration_t*) malloc(sizeof(jks_configuration_t));
    obj->burst_data_path = getenv("JKS_BURST_DATA_PATH");
    obj->kernel_data_path = getenv("JKS_KERNEL_DATA_PATH");
}

void
stop_jks_engine(int signal) {
    jks_log_info(__FUNCTION__,"You requested me to die!\n");
    jks_render_engine_request_termination(gbl_engine);
}

// ----------------------------------------------------------------------------
// Application entry point
// ----------------------------------------------------------------------------

int 
main() {
    jks_configuration_t* config = jks_configuration_parse();
    jks_log_info(__FUNCTION__,"Loaded the application configuration!");
    
    // Load needed buffer
    jks_float_array_t* burst_array = jks_float_array_load(config->burst_data_path);
    jks_log_info(__FUNCTION__,"Loaded the burst data!");
    jks_float_array_t* kernel_array = jks_float_array_load(config->kernel_data_path);
    jks_log_info(__FUNCTION__,"Loaded the kernel data!");
    
    // Create JKS engine
    gbl_engine = jks_render_engine_create(
        burst_array->data,
        burst_array->size,
        kernel_array->data,
        kernel_array->size
    );
    jks_log_info(__FUNCTION__,"Created the JKS engine!");
    
    // Prepare for controlled termination
    signal(SIGINT, stop_jks_engine);
    
    // Wait until the user wants me to die
    pthread_mutex_lock(&gbl_engine->killer_mutex);

    // Kill the JKS engine
    jks_render_engine_destroy(gbl_engine);
    
    jks_log_info(__FUNCTION__,"Application shutdown completed!");
    return 0;
}
