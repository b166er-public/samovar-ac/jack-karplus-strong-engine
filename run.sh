#!/bin/bash

./build.sh

export JKS_BURST_DATA_PATH="./data/44100/burst.dat"
export JKS_KERNEL_DATA_PATH="./data/44100/kernel.dat"

# Execute the program
./target/program
