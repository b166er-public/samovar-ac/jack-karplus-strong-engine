#!/bin/bash

jack_connect karplus_strong:pcm-out system:playback_1
jack_connect karplus_strong:pcm-out system:playback_2
jack_connect karplus_strong:midi-in system:midi_capture_1

jack_lsp --connections