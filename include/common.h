#pragma once

#define NUMBER_OF_MIDI_KEYS 128
#define MAX_NOTE_INSTANCES 128

typedef struct float_quad {
    float f0;
    float f1;
    float f2;
    float f3;
} float_quad_t;