#pragma once

typedef struct jks_float_array {
    unsigned int size;
    float* data;
} jks_float_array_t;

jks_float_array_t*
jks_float_array_load(const char* path);

void
jks_float_array_destroy(jks_float_array_t* obj);