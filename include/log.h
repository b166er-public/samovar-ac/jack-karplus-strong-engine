#pragma once

void
jks_log_info(const char* function, const char* message);

void
jks_log_warning(const char* function, const char* message);

void
jks_log_error(const char* function, const char* message);