#pragma once

#include <stdlib.h>

#include "ring_buffer.h"

typedef struct jks_fir_filter {
    unsigned int kernel_size;
    float* kernel;
    jks_ring_buffer_t* state;
    
    float* evaluate_cache;
} jks_fir_filter_t;

jks_fir_filter_t*
jks_fir_filter_create(const unsigned int kernel_size, const float* kernel);

void
jks_fir_filter_destroy(jks_fir_filter_t* obj);

float
jks_fir_filter_process_single(jks_fir_filter_t* obj, float input);

void
jks_fir_filter_process_array(
    jks_fir_filter_t* obj, 
    const float* input,
    const unsigned int input_size,
    float* output
);
