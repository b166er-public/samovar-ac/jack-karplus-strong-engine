#pragma once

#include "ring_buffer.h"
#include "fir_filter.h"

typedef struct jks_note_instance {
    float freq;
    jks_ring_buffer_t* memory;
    jks_fir_filter_t* filter;
    unsigned int silece_sample_counter;
} jks_note_instance_t;

jks_note_instance_t*
jks_note_instance_create(
    const float freq,
    const unsigned int sampling_rate,
    const float* burst,
    const unsigned int burst_size,
    const float* kernel,
    const unsigned int kernel_size
);

void
jks_note_instance_destroy(
    jks_note_instance_t* obj
);

float
jks_note_instance_render_single(
    jks_note_instance_t* obj
);

void
jks_note_instance_render_add_array(
    jks_note_instance_t* obj,
    float* output,
    const unsigned int output_size
);
