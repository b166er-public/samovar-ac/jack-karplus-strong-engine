#pragma once

#include <pthread.h>
#include <jack/jack.h>

#include "common.h"
#include "note_instance.h"

typedef struct jks_render_engine {
    // Common
    float* burst;
    unsigned int burst_size;
    float* kernel;
    unsigned int kernel_size;
    unsigned int sampling_rate;
    
    // MIDI State
    unsigned int midi_state_new[NUMBER_OF_MIDI_KEYS];
    unsigned int midi_state_old[NUMBER_OF_MIDI_KEYS];
    pthread_mutex_t midi_state_mutex;
    
    // Rendering buffer
    unsigned int buffer_size;
    float* buffer;
    pthread_mutex_t buffer_mutex;
    
    // Instance management
    jks_note_instance_t* instances[MAX_NOTE_INSTANCES];
    
    // Rendering request mutex
    pthread_mutex_t rendering_request_mutex;
    
    // Provider thread
    pthread_t provider_thread;
    
    // Consumer resources
    jack_client_t* jack_client_handle;
    jack_port_t* jack_pcm_output_port;
    jack_port_t* jack_midi_input_port;
    
    // Killer mutex
    pthread_mutex_t killer_mutex;

} jks_render_engine_t;


// Update MIDI delta
void
jks_render_engine_provider_update_delta(
    jks_render_engine_t* obj,
    unsigned int* midi_delta
);

// Start new note instance handler
void
jks_render_engine_provider_start_new_instance(
    jks_render_engine_t* obj,
    const float note_frequency,
    const float amplitude
);

// Rendering function
void*
jks_render_engine_provider_thread_function(void *arg);


jks_render_engine_t*
jks_render_engine_create(
    float* burst,
    unsigned int burst_size,
    float* kernel,
    unsigned int kernel_size
);

void
jks_render_engine_destroy(jks_render_engine_t* obj);

void
jks_render_engine_request_termination(jks_render_engine_t* obj);