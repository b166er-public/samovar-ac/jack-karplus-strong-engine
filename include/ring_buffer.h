#pragma once

#include "common.h"

typedef struct jks_ring_buffer {
    unsigned int size;
    unsigned int pos;
    float* data;
} jks_ring_buffer_t;

jks_ring_buffer_t*
jks_ring_buffer_create(unsigned int size);

void
jks_ring_buffer_destroy(jks_ring_buffer_t* obj);

void
jks_ring_buffer_init(
    jks_ring_buffer_t* obj,
    const unsigned int pos,
    const float* data
);

/* I/O single value */
float
jks_ring_buffer_read_single(const jks_ring_buffer_t* obj);

float
jks_ring_buffer_read_single_offset(
    const jks_ring_buffer_t* obj, 
    const unsigned int offset
);

void
jks_ring_buffer_write_single(jks_ring_buffer_t* obj, float value);

/* I/O quads */
float_quad_t
jks_ring_buffer_read_quad_offset(
    const jks_ring_buffer_t* obj, 
    const unsigned int offset
);

float_quad_t
jks_ring_buffer_read_quad(const jks_ring_buffer_t* obj);

void
jks_ring_buffer_write_quad(
    jks_ring_buffer_t* obj, 
    const float_quad_t value
);

/* I/O array */
void
jks_ring_buffer_read_array(
    const jks_ring_buffer_t* obj, 
    float* target, 
    const unsigned int target_size
);