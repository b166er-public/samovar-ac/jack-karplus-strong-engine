#!/bin/bash

# Clear & Create the target folder
rm -rf target
mkdir target

# Compile the generator
INCLUDE_FOLDERS="./include"
gcc -I${INCLUDE_FOLDERS} -g -c ./src/common.c -o ./target/common.o -pthread
gcc -I${INCLUDE_FOLDERS} -g -c ./src/fir_filter.c -o ./target/fir_filter.o -pthread
gcc -I${INCLUDE_FOLDERS} -g -c ./src/float_array.c -o ./target/float_array.o -pthread
gcc -I${INCLUDE_FOLDERS} -g -c ./src/log.c -o ./target/log.o -pthread
gcc -I${INCLUDE_FOLDERS} -g -c ./src/note_instance.c -o ./target/note_instance.o -pthread
gcc -I${INCLUDE_FOLDERS} -g -c ./src/program.c -o ./target/program.o -pthread
gcc -I${INCLUDE_FOLDERS} -g -c ./src/render_engine.c -o ./target/render_engine.o -pthread
gcc -I${INCLUDE_FOLDERS} -g -c ./src/ring_buffer.c -o ./target/ring_buffer.o -pthread

# Link the generator
OBJ_FILES="./target/common.o"
OBJ_FILES="${OBJ_FILES} ./target/fir_filter.o"
OBJ_FILES="${OBJ_FILES} ./target/float_array.o"
OBJ_FILES="${OBJ_FILES} ./target/log.o"
OBJ_FILES="${OBJ_FILES} ./target/note_instance.o"
OBJ_FILES="${OBJ_FILES} ./target/program.o"
OBJ_FILES="${OBJ_FILES} ./target/render_engine.o"
OBJ_FILES="${OBJ_FILES} ./target/ring_buffer.o"

gcc -g -o ./target/program -ljack -lm -pthread ${OBJ_FILES}

# Print the environment
# printenv

# Make generator executable
chmod +x ./target/program
